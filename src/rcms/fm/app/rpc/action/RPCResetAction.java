package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCResetAction extends RPCAbstractAction {

    public RPCResetAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.RESETTING.getStateString());

        eventHandler.getLogger().info("Running Halt for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.HALT, functionManager.getLastInput(), RPCStates.HALTED);
        eventHandler.getLogger().info("Running Reset for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.RESET, functionManager.getLastInput(), RPCStates.HALTED);

        eventHandler.getLogger().info("Running Reset for TS Cells");
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.RESET, functionManager.getLastInput(), RPCTSStates.HALTED);

        return RPCInputs.SETHALTED;
    }
}

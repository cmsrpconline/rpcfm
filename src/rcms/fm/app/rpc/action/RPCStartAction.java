package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSParameters;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCStartAction extends RPCAbstractAction {

    public RPCStartAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event) throws UserActionException {
        functionManager.setActionMessage(RPCStates.STARTING.getStateString());

        eventHandler.getLogger().info("Running Start for TS Cells");
        Input tsInput = RPCTSInputs.translateStart(functionManager.getLastInput());
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.START, tsInput, RPCTSStates.RUNNING);

        eventHandler.getLogger().info("Running Start for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.START, functionManager.getLastInput(), RPCStates.RUNNING);

        exportParameterSet(RPCInputs.START, functionManager.getLastInput().getParameterSet(), RPCParameters.STARTED_WITH);

        return RPCInputs.SETRUNNING;
    }
}

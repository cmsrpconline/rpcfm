package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCTestTTSAction extends RPCAbstractAction {

    public RPCTestTTSAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.TESTING_TTS.getStateString());

        eventHandler.getLogger().info("Running TTS Test for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.TEST_TTS, functionManager.getLastInput(), RPCStates.TTSTEST_MODE);

        exportParameterSet(RPCInputs.TEST_TTS, functionManager.getLastInput().getParameterSet(), RPCParameters.TTS_TESTED_WITH);

        return RPCInputs.SETTTSTEST_MODE;
    }
}

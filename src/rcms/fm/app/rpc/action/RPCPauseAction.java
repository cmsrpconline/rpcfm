package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCPauseAction extends RPCAbstractAction {

    public RPCPauseAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.PAUSING.getStateString());

        eventHandler.getLogger().info("Running Pause for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.PAUSE, functionManager.getLastInput(), RPCStates.PAUSED);

        eventHandler.getLogger().info("Running Pause for TS Cells");
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.PAUSE, functionManager.getLastInput(), RPCTSStates.PAUSED);

        return RPCInputs.SETPAUSED;
    }
}
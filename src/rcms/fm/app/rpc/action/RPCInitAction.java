package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.app.rpc.ts.TSCell;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.resource.QualifiedGroup;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;

public class RPCInitAction extends RPCAbstractAction {

    public RPCInitAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.INITIALIZING.getStateString());

        functionManager.checkIgnoreTS();

        QualifiedGroup qualifiedGroup = functionManager.getQualifiedGroup();

        // initialize the QualifiedGroup
        try {
            qualifiedGroup.init();
        } catch (Exception error) {
            String errMsg = this.getClass().toString() + " failed to initialize resources: " + error.getMessage();
            eventHandler.getLogger().error(errMsg, error);
            eventHandler.sendCMSError(errMsg);
            return RPCInputs.SETERROR;
        }

        functionManager.fillResourceContainers();

        // Initialize XDAQ Applications - Halt for DCC
        eventHandler.getLogger().info("Running Initialize for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.HALT, functionManager.getLastInput(), RPCStates.HALTED);

        // Initialize TSCells and reset them, since they're not controlled with JobControl for P5
        String sessionId = "-1";
        if (functionManager.getLastInput().getParameterSet().get(RPCParameters.SID) != null)
            sessionId = functionManager.getLastInput().getParameterSet().get(RPCParameters.SID).getValue().toString();
        for (TSCell tsCell : functionManager.getTSCellContainer().getCells())
            tsCell.setSessionId(sessionId);
        eventHandler.getLogger().info("Running Reset for TS Cells");
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.RESET, functionManager.getLastInput(), RPCTSStates.HALTED);


        // check states
        /*
          State xdaqState = functionManager.getXdaqApplicationContainer().compareAllActualStates(true);
          if (!xdaqState.equals(RPCStates.HALTED))
          throw new UserActionException("Failed to initialize Xdaq resources, retrieved state " + xdaqState.getStateString());
          State tsState = functionManager.getTSCellContainer().compareAllActualStates(true);
          if (!tsState.equals(RPCTSStates.HALTED))
          throw new UserActionException("Failed to initialize TS resources, retrieved state " + tsState.getStateString());
        */

        return RPCInputs.SETHALTED;
    }
}

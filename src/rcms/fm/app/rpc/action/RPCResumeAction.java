package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCResumeAction extends RPCAbstractAction {

    public RPCResumeAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.RESUMING.getStateString());

        eventHandler.getLogger().info("Running Resume for TS Cells");
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.RESUME, functionManager.getLastInput(), RPCTSStates.RUNNING);

        eventHandler.getLogger().info("Running Resume for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.RESUME, functionManager.getLastInput(), RPCStates.RUNNING);

        return RPCInputs.SETRUNNING;
    }
}

package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCPrepareTTSTestModeAction extends RPCAbstractAction {

    public RPCPrepareTTSTestModeAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.PREPARING_TTSTEST_MODE.getStateString());

        eventHandler.getLogger().info("Running PrepareTTSTestMode for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.TTSTEST_MODE, functionManager.getLastInput(), RPCStates.TTSTEST_MODE);

        return RPCInputs.SETTTSTEST_MODE;
    }
}
package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.ts.*;

import java.util.Map;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.RPCFunctionManager;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.tools.ParameterTools;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter.Exported;
import rcms.fm.fw.parameter.Parameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.ParameterType;
import rcms.fm.fw.service.parameter.ParameterServiceException;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.resource.QualifiedResource;
import rcms.fm.resource.QualifiedResourceContainer;
import rcms.fm.resource.QualifiedResourceContainerException;
import rcms.fm.resource.qualifiedresource.XdaqApplication;
import rcms.fm.resource.qualifiedresource.XdaqApplicationContainer;
import rcms.stateFormat.StateNotification;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;

public abstract class RPCAbstractAction implements RPCAction {

    RPCEventHandler eventHandler;
    RPCFunctionManager functionManager;

    public RPCAbstractAction(final RPCEventHandler eventHandler)
    {
        this.eventHandler = eventHandler;
        this.functionManager = eventHandler.getFunctionManager();
    }

    public final void handle(final Object event) throws UserActionException {
        eventHandler.getLogger().info("Handle event: " + event);

        Input input = null;

        if (event instanceof StateNotification) {
            // triggered by State Notification from child resource
            input = onStateNotification((StateNotification)event);
        } else if (event instanceof StateEnteredEvent) {
            // triggered by entered state action
            try {
                input = onStateEnteredEvent((StateEnteredEvent)event);
            } catch (UserActionException error) {
                eventHandler.getLogger().error(error);
                eventHandler.sendCMSError(error);
                input = RPCInputs.SETERROR;
            }
        }

        if (input != null) {
            eventHandler.getLogger().info("Going to next state: " + input);
            if (RPCInputs.SETERROR.equals(input)) {
                eventHandler.getFunctionManager().setActionMessage("");
                eventHandler.getFunctionManager().firePriorityEvent(input);
            } else {
                eventHandler.getFunctionManager().setActionMessage("");
                eventHandler.getFunctionManager().fireEvent(input);
            }
        } else {
            eventHandler.getLogger().info("Handling Action resulted in null input.  Doing nothing.");
        }

        eventHandler.getLogger().info("Handled event: " + event);
        return;
    }

    protected Input onStateNotification(final StateNotification event) throws UserActionException
    {
        eventHandler.getLogger().info("Ignoring StateNotification: " + (event.getFromState() + " to " + event.getToState()));
        return null;
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event) throws UserActionException
    {
        eventHandler.getLogger().info("Ignoring StateEnteredEvent: " + event);
        return null;
    }

    protected void sendInput(final QualifiedResourceContainer qrcontainer
                             , final Input definition, final Input input, final State result)
        throws UserActionException
    {
        if (qrcontainer == null || qrcontainer.isEmpty())
            return;
        Input qrInput = new Input(definition.getInputString());
        Map<QualifiedResource, State> states;
        try {
            if (definition.getParameterSet() != null) {
                if (input.getParameterSet() == null)
                    qrInput.setParameters(new ParameterSet<CommandParameter>(definition.getParameterSet()));
                else
                    qrInput.setParameters(ParameterTools.translateCommandParameterSet(definition.getParameterSet(), input.getParameterSet()));
            }
            states = qrcontainer.execute(qrInput);
        } catch (QualifiedResourceContainerException error) {
            throw new UserActionException("Error executing input \"" + qrInput.getInputString()
                                          + "\" for Qualified Resources of type \""
                                          +  (qrcontainer.getQualifiedResourceList().iterator().next().getResource().getQualifiedResourceType()) + "\""
                                          , error);
        }

        if (!QualifiedResourceContainer.compareAllStates(states).equals(result))
            throw new UserActionException("Error executing input \"" + qrInput.getInputString()
                                          + "\" for Qualified Resources of type \""
                                          +  (qrcontainer.getQualifiedResourceList().iterator().next().getResource().getQualifiedResourceType())
                                          + "\": new states aren't \"" + result.getStateString() + "\"");
    }

    protected void exportParameterSet(final Input input
                                      , final ParameterSet<CommandParameter> commandParameterSet
                                      , final String prefix)
    {
        if (input.getParameterSet() == null)
            return;

        // copy the matching parameters.
        for (Map.Entry<String, CommandParameter> parameter : input.getParameterSet().getMap().entrySet()) {
            try {
                functionManager.getParameterSet().put(new FunctionManagerParameter<ParameterType>(prefix + parameter.getKey()
                                                                                                  , commandParameterSet.get(parameter.getKey()).getValue(), Exported.READONLY));
            } catch (Exception error) {
                eventHandler.getLogger().warn("Exception exporting Command Parameter " + parameter.getKey()
                                              + ".  Using default value \"" + input.getParameterSet().get(parameter.getKey()).getValue().toString() + "\".");
                try {
                    functionManager.getParameterSet().put(new FunctionManagerParameter<ParameterType>(prefix + parameter.getKey()
                                                                                                      , parameter.getValue().getValue(), Exported.READONLY));
                } catch (Exception secondError) {
                    eventHandler.getLogger().warn("Exception exporting Command Parameter " + parameter.getKey()
                                                  + " with the default value, giving up."
                                                 , error);
                }
            }
        }
    }
}

package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCStopAction extends RPCAbstractAction {

    public RPCStopAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.STOPPING.getStateString());

        eventHandler.getLogger().info("Running Stop for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.STOP, functionManager.getLastInput(), RPCStates.CONFIGURED);

        eventHandler.getLogger().info("Running Stop for TS Cells");
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.STOP, functionManager.getLastInput(), RPCTSStates.CONFIGURED);

        return RPCInputs.SETCONFIGURED;

    }
}

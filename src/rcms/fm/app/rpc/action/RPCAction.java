package rcms.fm.app.rpc.action;

import rcms.fm.fw.user.UserActionException;

public interface RPCAction {
    void handle(Object event) throws UserActionException;
}

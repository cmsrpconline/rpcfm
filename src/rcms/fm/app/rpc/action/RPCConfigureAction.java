package rcms.fm.app.rpc.action;

import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.definition.RPCInputs;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.definition.RPCTSParameters;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.user.UserActionException;
import rcms.statemachine.definition.Input;

public class RPCConfigureAction extends RPCAbstractAction {

    public RPCConfigureAction(final RPCEventHandler eventHandler)
    {
        super(eventHandler);
    }

    protected Input onStateEnteredEvent(final StateEnteredEvent event)
        throws UserActionException
    {
        functionManager.setActionMessage(RPCStates.CONFIGURING.getStateString());

        functionManager.checkIgnoreTS();

        eventHandler.getLogger().info("Running Configure for TS Cells");
        Input tsInput = RPCTSInputs.translateConfigure(functionManager.getLastInput());
        sendInput(functionManager.getTSCellContainer()
                  , RPCTSInputs.CONFIGURE, tsInput, RPCTSStates.CONFIGURED);

        eventHandler.getLogger().info("Running Configure for Xdaq Applications");
        sendInput(functionManager.getXdaqApplicationContainer()
                  , RPCInputs.CONFIGURE, functionManager.getLastInput(), RPCStates.CONFIGURED);

        exportParameterSet(RPCInputs.CONFIGURE, functionManager.getLastInput().getParameterSet(), RPCParameters.CONFIGURED_WITH);

        return RPCInputs.SETCONFIGURED;
    }
}

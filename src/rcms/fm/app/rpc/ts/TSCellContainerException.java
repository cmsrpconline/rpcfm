package rcms.fm.app.rpc.ts;

import rcms.fm.resource.QualifiedResourceContainerException;

public class TSCellContainerException extends QualifiedResourceContainerException
{
    public TSCellContainerException() {}
    public TSCellContainerException(String message) { super(message); }
    public TSCellContainerException(String message, Throwable cause) {
        super(message, cause);
    }
}

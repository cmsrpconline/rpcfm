package rcms.fm.app.rpc.ts;

public class TSCellCommandResponseException extends TSCellCommandException
{
    public TSCellCommandResponseException() {}
    public TSCellCommandResponseException(String message) { super(message); }
    public TSCellCommandResponseException(Throwable cause) { super(cause); }
    public TSCellCommandResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}

package rcms.fm.app.rpc.ts;

public class TSCellCommandRequestException extends TSCellCommandException
{
    public TSCellCommandRequestException() {}
    public TSCellCommandRequestException(String message) { super(message); }
    public TSCellCommandRequestException(Throwable cause) { super(cause); }
    public TSCellCommandRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}

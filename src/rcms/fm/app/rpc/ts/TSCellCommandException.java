package rcms.fm.app.rpc.ts;

import net.hep.cms.xdaqctl.XDAQMessageException;

public class TSCellCommandException extends XDAQMessageException
{
    public TSCellCommandException() {}
    public TSCellCommandException(String message) { super(message); }
    public TSCellCommandException(Throwable cause) { super(cause); }
    public TSCellCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}

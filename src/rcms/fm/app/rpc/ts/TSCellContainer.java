package rcms.fm.app.rpc.ts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rcms.fm.resource.CommandException;
import rcms.fm.resource.QualifiedResource;
import rcms.fm.resource.QualifiedResourceContainer;
import rcms.fm.resource.QualifiedResourceContainerException;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;

// copied from XdaqApplicationContainer
// consider dropping it
public class TSCellContainer extends QualifiedResourceContainer {

    private List<TSCell> _TSCellList = null;

    public TSCellContainer(List<? extends QualifiedResource> cellList)
    {
        // call QualifiedResource constructor
        super(cellList);

        _TSCellList = new ArrayList<TSCell>();

        // check argument
        if (cellList != null) {
            for (QualifiedResource qr : cellList) {
                if (qr instanceof TSCell) {
                    _TSCellList.add((TSCell)qr);
                }
            }
        }
    }

    public Map<QualifiedResource, State> execute(Input input, String cellClass)
        throws TSCellContainerException
    {
        // construct list which contains the selected TSCells
        List<TSCell> tsCellSelectionList = new ArrayList<TSCell>();

        // loop over the list and execute the command
        for (TSCell app : _TSCellList) {
            if (app.getApplication().equals(cellClass)) {
                tsCellSelectionList.add(app);
            }
        }

        // make a temporary container with the selected TSCells
        QualifiedResourceContainer tmpQRC =
            new QualifiedResourceContainer(tsCellSelectionList);

        // execute the command and return the result Map
        Map<QualifiedResource, State> map = null;
        try {
            map = tmpQRC.execute(input);
        }
        catch (QualifiedResourceContainerException qrce) {
            throw new TSCellContainerException("Could not execute commands",qrce);
        }

        return map;
    }


    public Map<QualifiedResource, State> execute(Input input, String cellClass, int instance)
        throws TSCellContainerException
    {
        // construct list which contains the selected TSCells
        List<TSCell> tsCellSelectionList = new ArrayList<TSCell>();

        // loop over the list and execute the command
        for (TSCell app : _TSCellList) {
            if (app.getApplication().equals(cellClass) &&
                app.getInstance().equals(instance + "")) {
                tsCellSelectionList.add(app);
            }
        }

        // make a temporary container with the selected TSCells
        QualifiedResourceContainer tmpQRC =
            new QualifiedResourceContainer(tsCellSelectionList);

        // execute the command and return the result Map
        Map<QualifiedResource, State> map = null;
        try {
            map = tmpQRC.execute(input);
        }
        catch (QualifiedResourceContainerException qrce) {
            throw new TSCellContainerException("Could not execute commands.",qrce);
        }
        return map;

    }


    public List<String> getCellClasses()
    {
        List<String> result = new ArrayList<String>();

        for (TSCell app : _TSCellList) {
            if (!result.contains(app.getApplication())) {
                result.add(app.getApplication());
            }
        }

        return result;
    }

    public List<Integer> getInstanceNumbersOfClass(String cellClass)
    {
        List<Integer> result = new ArrayList<Integer>();

        for (TSCell app : _TSCellList) {
            if (app.getApplication().equals(cellClass) &&
                !result.contains(app.getInstance())
                ) {
                result.add(new Integer(app.getInstance()));
            }
        }

        return result;
    }

    public List<TSCell> getCellsOfClass(String cellClass) {

        List<TSCell> result = new ArrayList<TSCell>();

        for (TSCell app : _TSCellList) {
            if (
                // check for a valid cell name
                app.getApplication() != null &&
                // check for cell name
                app.getApplication().equals(cellClass)
                )
                {
                    result.add(app);
                }
        }

        return result;
    }

    public List<TSCell> getCells() {
        return _TSCellList;
    }
}



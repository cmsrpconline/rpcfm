package rcms.fm.app.rpc.ts;

import rcms.fm.app.rpc.tools.ParameterTools;
import rcms.fm.app.rpc.tools.ParameterToolsException;

import java.util.Iterator;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import rcms.fm.fw.parameter.Parameter;
import rcms.util.logger.RCMSLogger;

public class TSCellCommandResponse
{
    static RCMSLogger logger = new RCMSLogger(TSCellCommandResponse.class);

    private String commandName;
    private boolean hasFault = false;
    private String faultCode;
    private String faultString;
    private TSCellWarning warning = null;
    private Parameter payload;

    public TSCellCommandResponse()
    {}

    public String getCommandName()
    {
        return commandName;
    }

    public boolean hasFault()
    {
        return hasFault;
    }

    public String getFaultCode()
    {
        return faultCode;
    }

    public String getFaultString()
    {
        return faultString;
    }

    public boolean hasWarning()
    {
        return (warning != null);
    }

    public TSCellWarning getWarning()
    {
        return warning;
    }

    public Parameter getPayload()
    {
        return payload;
    }

    public void reset()
    {
        commandName = null;
        hasFault = false;
        faultCode = null;
        faultString = null;
        warning = null;
        payload = null;
    }

    public TSCellCommandResponse parse(SOAPMessage message, final String expectedCommandName)
        throws TSCellCommandResponseException
    {
        logger.debug("TSCellCommandResponse constructor with " + message + " and " + expectedCommandName);

        reset();

        // get Body
        SOAPBody body = null;
        try {
            body = message.getSOAPBody();
        } catch (Exception error) {
            logger.error("Received incomplete SOAPMessage in TSCellCommandResponse", error);
            throw new TSCellCommandResponseException("Received incomplete SOAPMessage in TSCellCommandResponse", error);
        }

        if (body == null)
            {
                logger.error("Received incomplete SOAPMessage in TSCellCommandResponse: null body");
                throw new TSCellCommandResponseException("Received incomplete SOAPMessage in TSCellCommandResponse: null body");
            }
        // process Body
        Iterator bodyIterator = body.getChildElements();
        while (bodyIterator.hasNext()) {
            try {
                SOAPElement bodyElement = (SOAPElement)bodyIterator.next();
                if (bodyElement == null)
                    continue;

                String bodyLocalName = bodyElement.getLocalName();

                if (bodyLocalName.equals("Fault")) { // process Fault
                    hasFault = true;
                    try {
                        SOAPFault fault = (SOAPFault)bodyElement;
                        faultCode = fault.getFaultCode();
                        faultString = fault.getFaultString();
                    } catch (Exception error) {
                        faultCode = "Exception getting FaultCode";
                        faultString = "Exception getting FaultString";
                    }
                } else if (expectedCommandName == null || bodyLocalName.equals(expectedCommandName)) { // process Response
                    if (commandName == null)
                        commandName = bodyElement.getLocalName();
                    else {
                        logger.error("Unexpected second SOAP body element " + bodyLocalName + ". Ignoring its content.");
                        continue;
                    }

                    Iterator responseIterator = bodyElement.getChildElements();
                    while (responseIterator.hasNext()) {
                        try {
                            SOAPElement responseElement = (SOAPElement)responseIterator.next();
                            if (responseElement == null)
                                continue;

                            String responseLocalName = responseElement.getLocalName();
                            if (responseLocalName.equals("payload")) { // process payload
                                try {
                                    payload = ParameterTools.createParameter(responseElement);
                                } catch (ParameterToolsException error) {
                                    logger.error("Received ParameterToolsException creating payload from element", error);
                                }
                            } else if (responseLocalName.equals("warningLevel")) { // process warningLevel
                                if (warning == null)
                                    warning = new TSCellWarning();
                                try {
                                    warning.setLevel(Integer.parseInt(responseElement.getValue()));
                                } catch (NumberFormatException error) {
                                    logger.error("Received NumberFormatException while parsing TSCellCommandResponse warningLevel.");
                                }
                            } else if (responseLocalName.equals("warningMessage")) { // process warningMessage
                                if (warning == null)
                                    warning = new TSCellWarning();
                                warning.setMessage(responseElement.getValue());
                            } else {
                                logger.error("Found unexpected element while processing TSCellCommandResponse response: " + responseLocalName);
                            }
                        } catch (Exception error) {
                            logger.error("Exception while processing TSCellCommandResponse response", error);
                        }
                    }
                } else if (commandName != null)
                    logger.error("Found unexpected element while processing TSCellCommandResponse body: " + commandName);
            } catch (Exception error) {
                logger.error("Exception while processing TSCellCommandResponse body", error);
            }
        }
        return this;
    }
}

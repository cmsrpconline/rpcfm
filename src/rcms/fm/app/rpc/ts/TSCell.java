package rcms.fm.app.rpc.ts;

// todo: replace logger errors with Warnings, so they can be caught and added to the ERROR_MSG

import rcms.fm.resource.qualifiedresource.XdaqApplication;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.soap.SOAPMessage;
import net.hep.cms.xdaqctl.XDAQMessageException;
import net.hep.cms.xdaqctl.XDAQTimeoutException;
import org.w3c.dom.Document;
import rcms.fm.fw.parameter.Parameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.ParameterType;
import rcms.fm.resource.CommandException;
import rcms.fm.resource.CommandTimeoutException;
import rcms.resourceservice.db.resource.Resource;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;
import rcms.statemachine.definition.Transition;
import rcms.util.logger.RCMSLogger;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import javax.xml.transform.dom.DOMSource;
import javax.xml.soap.SOAPException;

public class TSCell extends XdaqApplication
{
    protected static final State unknownState = new State("N/A");

    static RCMSLogger logger = new RCMSLogger(TSCell.class);

    protected String mainOperation = "Configuration";
    protected String sessionId = "-1";

    protected TSCellWarning warning;

    protected Map<Input, Transition> transitions = new HashMap<Input, Transition>();
    protected Map<Input, Integer> completionTimeoutsInSeconds = new HashMap<Input, Integer>();
    int completionTimeoutInSeconds = 120;

    public TSCell()
    {
        super();
    }

    public TSCell(Resource resource)
    {
        super(resource);
    }

    public TSCell(XdaqApplication xdaqApplication)
    {
        super(xdaqApplication.getResource());
    }

    public String getMainOperation()
    {
        return mainOperation;
    }

    public void setMainOperation(String mainOperation)
    {
        this.mainOperation = mainOperation;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public TSCellWarning getWarning()
    {
        return warning;
    }

    public void addTransition(Transition transition)
    {
        transitions.put(transition.getInput(), transition);
    }

    public void addTransition(Transition transition, int completionTimeoutInSeconds)
    {
        transitions.put(transition.getInput(), transition);
        completionTimeoutsInSeconds.put(transition.getInput(), completionTimeoutInSeconds);
    }

    public Transition getTransition(Input input)
    {
        return transitions.get(input);
    }

    public int getCompletionTimeout(Input input)
    {
        Integer timeout = completionTimeoutsInSeconds.get(input);
        if (timeout == null)
            return getCompletionTimeout();
        return timeout;
    }

    public int getCompletionTimeout()
    {
        return completionTimeoutInSeconds;
    }

    public void setCompletionTimeout(int completionTimeoutInSeconds)
    {
        this.completionTimeoutInSeconds = completionTimeoutInSeconds;
    }

    public synchronized State refreshState()
    {
        return refreshState(mainOperation);
    }

    public synchronized State refreshState(String operation)
    {
        TSCellCommand getStateCommand = null;
        try {
            getStateCommand = new TSCellCommand("OpGetState", sessionId);
        } catch (TSCellCommandException error) {
            logger.error("Failed to conctruct TSCellCommand in TSCell refreshState (TSCellCommandException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        } catch (XDAQMessageException error) {
            logger.error("Failed to conctruct TSCellCommand in TSCell refreshState (XDAQMessageException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        }
        if (getStateCommand == null) {
            logger.error("Failed to conctruct TSCellCommand in TSCell refreshState to " + this.getURI().toString());
            return unknownState;
        }

        try {
            getStateCommand.getRequest().addElement("operation", operation);
            command(getStateCommand);
            TSCellCommandResponse response = getStateCommand.getResponse();
            if (response == null) {
                logger.error("Null response for TSCell refreshState to " + this.getURI().toString());
                return unknownState;
            }
            if (response.hasFault()) {
                logger.error("Fault in TSCell refreshState response to " + this.getURI().toString()
                             + ": " + response.getFaultString());
                return unknownState;
            }
            if (response.hasWarning()) {
                warning = response.getWarning();
                if (warning.getLevel() > TSCellWarning.WARNING) {
                    logger.error("Warning in TSCell refreshState response to " + this.getURI().toString()
                                 + ": " + warning);
                    return unknownState;
                } else if (warning.getLevel() > TSCellWarning.INFO) {
                    logger.warn("Warning in TSCell refreshState response to " + this.getURI().toString()
                                + ": " + warning);
                } else
                    logger.info("Warning in TSCell refreshState response to " + this.getURI().toString()
                                + ": " + warning);
            }
            Parameter payload = response.getPayload();
            if (payload == null) {
                logger.error("Missing payload for TSCell refreshState to " + this.getURI().toString());
                return unknownState;
            }
            setCacheState(new State(payload.getValue().toString()));
            return getCacheState();
        } catch (TSCellCommandException error) {
            logger.error("Failed to run TSCellCommand in refreshState (TSCellCommandException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        } catch (TSCellException error) {
            logger.error("Failed to run TSCellCommand in refreshState (TSCellException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        }
    }

    public synchronized State waitForState(final Input input, int timeoutInSeconds)
    {
        Transition transition = transitions.get(input);
        return waitForState(transition, timeoutInSeconds);
    }

    public synchronized State waitForState(Transition transition, int timeoutInSeconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, timeoutInSeconds);

        String from = null;
        if (transition.getFrom() != null)
            from = transition.getFrom().getStateString() + "_" + transition.getInput().getInputString() + "_" + transition.getTo().getStateString();
        do {
            State state = refreshState();
            logger.debug("waitForState: " + state.getStateString() + " vs " + from);
            if (state.equals(transition.getTo()))
                return transition.getTo();
            if (from != null && !state.getStateString().equals(from))
                return state;

            try {
                Thread.sleep(2000);
            } catch (Exception error) {
            }
        } while (Calendar.getInstance().compareTo(calendar) < 0);

        return refreshState();
    }

    public synchronized State executeReset(final Input input, String operation)
    {
        TSCellCommand resetCommand = null;
        try {
            resetCommand = new TSCellCommand("OpReset", sessionId);
        } catch (TSCellCommandException error) {
            logger.error("Failed to conctruct TSCellCommand in TSCell executeReset (TSCellCommandException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        } catch (XDAQMessageException error) {
            logger.error("Failed to conctruct TSCellCommand in TSCell executeReset (XDAQMessageException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        }
        if (resetCommand == null) {
            logger.error("Failed to conctruct TSCellCommand in TSCell executeReset to " + this.getURI().toString());
            return unknownState;
        }

        try {
            // async with bogus uri and urn, since nobody's waiting for the response
            // consider using rcms notificationreceiver
            resetCommand.getRequest().addElement("operation", operation).setAsync(true, getURL().toString(), getURN().toString());
            command(resetCommand);
            TSCellCommandResponse response = resetCommand.getResponse();
            if (response == null) {
                logger.error("Null response for TSCell executeReset to " + this.getURI().toString());
                return unknownState;
            }
            if (response.hasFault()) {
                logger.error("Fault in TSCell executeReset response to " + this.getURI().toString()
                             + ": " + response.getFaultString());
                return unknownState;
            }
            if (response.hasWarning()) {
                warning = response.getWarning();
                if (warning.getLevel() > TSCellWarning.WARNING) {
                    logger.error("Warning in TSCell executeReset response to " + this.getURI().toString()
                                 + ": " + warning);
                    return unknownState;
                } else if (warning.getLevel() > TSCellWarning.INFO) {
                    logger.warn("Warning in TSCell executeReset response to " + this.getURI().toString()
                                + ": " + warning);
                } else
                    logger.info("Warning in TSCell executeReset response to " + this.getURI().toString()
                                + ": " + warning);
            }
        } catch (TSCellCommandException error) {
            logger.error("Failed to run TSCellCommand in executeReset (TSCellCommandException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        } catch (TSCellException error) {
            logger.error("Failed to run TSCellCommand in executeReset (TSCellException) to "
                         + this.getURI().toString(), error);
            return unknownState;
        }

        return waitForState(input, getCompletionTimeout(input));
    }

    // from QualifiedResource
    public synchronized State execute(final Input input)
    {
        return execute(input, mainOperation);
    }

    public synchronized State execute(final Input input, String operation)
    {
        if (input.getInputString().equals("reset"))
            return executeReset(input, operation);

        TSCellCommand inputCommand = null;
        try {
            inputCommand = new TSCellCommand(input, operation, sessionId);
        } catch (TSCellCommandException error) {
            logger.error("Failed to conctruct TSCellCommand in execute (TSCellCommandException) for input "
                         + input.getInputString() + " to " + this.getURI().toString(), error);
            return unknownState;
        } catch (XDAQMessageException error) {
            logger.error("Failed to conctruct TSCellCommand in execute (XDAQMessageException) for input "
                         + input.getInputString() + " to " + this.getURI().toString(), error);
            return unknownState;
        }
        if (inputCommand == null) {
            logger.error("Failed to conctruct TSCellCommand in execute for input "
                         + input.getInputString() + " to " + this.getURI().toString());
            return unknownState;
        }

        try {
            // async with bogus uri and urn, since nobody's waiting for the response
            // consider using rcms notificationreceiver
            inputCommand.getRequest().setAsync(true, getURI().toString(), getURN().toString());
            command(inputCommand);
            TSCellCommandResponse response = inputCommand.getResponse();
            if (response == null) {
                logger.error("Null response for TSCell execute for input "
                             + input.getInputString() + " to " + this.getURI().toString());
                return unknownState;
            }
            if (response.hasFault()) {
                logger.error("Fault in TSCell execute response for input "
                             + input.getInputString() + " to " + this.getURI().toString()
                             + ": " + response.getFaultString());
                return unknownState;
            }
            if (response.hasWarning()) {
                warning = response.getWarning();
                if (warning.getLevel() > TSCellWarning.WARNING) {
                    logger.error("Warning in TSCell execute response to " + this.getURI().toString()
                                 + ": " + warning);
                    return unknownState;
                } else if (warning.getLevel() > TSCellWarning.INFO) {
                    logger.warn("Warning in TSCell execute response to " + this.getURI().toString()
                                + ": " + warning);
                } else
                    logger.info("Warning in TSCell execute response to " + this.getURI().toString()
                                + ": " + warning);
            }
        } catch (TSCellCommandException error) {
            logger.error("Failed to run TSCellCommand in execute (TSCellCommandException) for input "
                         + input.getInputString() + " to " + this.getURI().toString(), error);
            return unknownState;
        } catch (TSCellException error) {
            logger.error("Failed to run TSCellCommand in execute (TSCellException) for input "
                         + input.getInputString() + " to " + this.getURI().toString(), error);
            return unknownState;
        }

        return waitForState(input, getCompletionTimeout(input));
    }

    public Document command(final TSCellCommand tsCellCommand)
        throws TSCellException
    {
        Document response = null;

        try {
            tsCellCommand.setTimeout(getExecutionTimeout());
            response = tsCellCommand.send(this.getURL().toString(), this.getLid());

            try {
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                StringWriter writer = new StringWriter();
                StreamResult result = new StreamResult(writer);
                transformer.transform(new DOMSource(tsCellCommand.getRequest().getSOAPMessage().getSOAPBody()), result);
                logger.debug("request to TSCell is \n" + writer.getBuffer().toString());
                writer.getBuffer().setLength(0);
                transformer.transform(new DOMSource(response), result);
                logger.debug("response from TSCell is \n" + writer.getBuffer().toString());
            } catch (SOAPException error) {
                throw new TSCellException("Exception during command", error);
            } catch (TransformerConfigurationException error) {
                throw new TSCellException("Exception during command", error);
            } catch (TransformerException error) {
                throw new TSCellException("Exception during command", error);
            }

            tsCellCommand.setResponse(response);
            return response;

        } catch (XDAQMessageException error) {
            throw new TSCellException("Exception during command", error);
        } catch (XDAQTimeoutException error) {
            throw new TSCellException("Exception during command", error);
        } catch (Exception error) {
            throw new TSCellException("Exception during command", error);
        }
    }
}

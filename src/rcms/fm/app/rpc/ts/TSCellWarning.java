package rcms.fm.app.rpc.ts;

public class TSCellWarning
{
    public static int NO_WARNING = 0;
    public static int INFO = 1000;
    public static int WARNING = 2000;
    public static int ERROR = 3000;
    public static int FATAL = 4000;

    protected int level = 0;
    protected String message = "TSCellWarning missing message";

    public TSCellWarning()
    {}

    public TSCellWarning(int level, String message)
    {
        this.level = level;
        this.message = message;
    }

    public int getLevel()
    {
        return level;
    }

    public String getMessage()
    {
        return message;
    }

    public TSCellWarning setLevel(int level)
    {
        this.level = level;
        return this;
    }

    public TSCellWarning setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public String toString()
    {
        return "TSCellWarning(" + level + ", " + message + ")";
    }
};

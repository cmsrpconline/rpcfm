package rcms.fm.app.rpc.ts;

import rcms.fm.resource.qualifiedresource.XdaqApplicationException;

public class TSCellException extends XdaqApplicationException
{
    public TSCellException() {}
    public TSCellException(String message) { super(message); }
    public TSCellException(String message, Throwable cause) {
        super(message, cause);
    }
}

package rcms.fm.app.rpc.ts;

import java.util.Iterator;
import java.util.UUID;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import rcms.fm.fw.parameter.Parameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.statemachine.definition.Input;
import rcms.util.logger.RCMSLogger;

public class TSCellCommandRequest
{
    static RCMSLogger logger = new RCMSLogger(TSCellCommandRequest.class);

    protected SOAPMessage message = null;
    protected SOAPEnvelope envelope = null;
    protected SOAPElement command = null;
    protected SOAPElement payload = null;
    protected String commandName;

    public TSCellCommandRequest(final String commandName, String sessionId)
        throws TSCellCommandRequestException
    {
        initialize(commandName, sessionId);
    }

    public TSCellCommandRequest(final Input input, String operation, String sessionId)
        throws TSCellCommandRequestException
    {
        initialize("OpSendCommand", sessionId);
        addElement("command", input.getInputString());
        addElement("operation", operation);
        addParameters(input.getParameterSet());
    }

    public SOAPMessage getSOAPMessage()
    {
        return message;
    }

    public String getCommandName()
    {
        if (command != null)
            return command.getElementName().getLocalName();
        return null;
    }

    public TSCellCommandRequest addAttribute(final String name, final String value)
        throws TSCellCommandRequestException
    {
        try {
            command.addAttribute(envelope.createName(name), value);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest addAttribute" + error);
        }
        return this;
    }

    public TSCellCommandRequest setAttribute(final String name, final String value)
        throws TSCellCommandRequestException
    {
        try {
            command.removeAttribute(envelope.createName(name));
            command.addAttribute(envelope.createName(name), value);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest setAttribute" + error);
        }
        return this;
    }

    public TSCellCommandRequest addElement(final String name, final String value)
        throws TSCellCommandRequestException
    {
        return addElement(name, value, "xsd:string");
    }

    public TSCellCommandRequest addElement(final Parameter parameter)
        throws TSCellCommandRequestException
    {
        return addElement(parameter.getName(), parameter.getValue().toString(), parameter.getValue().getXSIType());
    }

    public TSCellCommandRequest addElement(final String name, final String value, final String type)
        throws TSCellCommandRequestException
    {
        try {
            SOAPElement element = command.addChildElement(envelope.createName(name, "cell", TSCellCommand.TS_NS_URI));
            element.addAttribute(envelope.createName("type", "xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI), type);
            element.addTextNode(value);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest addElement" + error);
        }
        return this;
    }

    public TSCellCommandRequest setElement(final String name, final String value)
        throws TSCellCommandRequestException
    {
        return setElement(name, value, "xsd:string");
    }

    public TSCellCommandRequest setElement(final Parameter parameter)
        throws TSCellCommandRequestException
    {
        return setElement(parameter.getName(), parameter.getValue().toString(), parameter.getValue().getXSIType());
    }

    public TSCellCommandRequest setElement(final String name, final String value, final String type)
        throws TSCellCommandRequestException
    {
        try {
            Iterator iterator = command.getChildElements(envelope.createName(name, "cell", TSCellCommand.TS_NS_URI));
            if (!iterator.hasNext())
                addElement(name, value, type);
            else {
                SOAPElement element = (SOAPElement)iterator.next();
                element.removeContents();
                element.addAttribute(envelope.createName("type", "xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI), type);
                element.addTextNode(value);
            }
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest setElement" + error);
        } catch (NullPointerException error) {
            throw new TSCellCommandRequestException("Received NullPointerException in TSCellCommandRequest setElement" + error);
        }
        return this;
    }

    public TSCellCommandRequest addParameter(final String name, final String value)
        throws TSCellCommandRequestException
    {
        return addParameter(name, value, "xsd:string");
    }

    public TSCellCommandRequest addParameter(final Parameter parameter)
        throws TSCellCommandRequestException
    {
        return addParameter(parameter.getName(), parameter.getValue().toString(), parameter.getValue().getXSIType());
    }

    public TSCellCommandRequest addParameter(final String name, final String value, final String type)
        throws TSCellCommandRequestException
    {
        try {
            SOAPElement paramElement = command.addChildElement(envelope.createName("param", "cell", TSCellCommand.TS_NS_URI));
            paramElement.addAttribute(envelope.createName("type", "xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI), type);
            paramElement.addAttribute(envelope.createName("name"), name);
            paramElement.addTextNode(value);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest addParameter" + error);
        }
        return this;
    }

    public TSCellCommandRequest setParameter(final String name, final String value)
        throws TSCellCommandRequestException
    {
        return setParameter(name, value, "xsd:string");
    }

    public TSCellCommandRequest setParameter(final Parameter parameter)
        throws TSCellCommandRequestException
    {
        return setParameter(parameter.getName(), parameter.getValue().toString(), parameter.getValue().getXSIType());
    }

    public TSCellCommandRequest setParameter(final String name, final String value, final String type)
        throws TSCellCommandRequestException
    {
        try {
            Iterator iterator = command.getChildElements(envelope.createName("param", "cell", TSCellCommand.TS_NS_URI));
            Name paramName = envelope.createName("name");
            while (iterator.hasNext()) {
                SOAPElement paramElement = (SOAPElement)iterator.next();
                if (paramElement.getAttributeValue(paramName).equals(name)) {
                    paramElement.removeContents();
                    paramElement.addAttribute(paramName, name);
                    paramElement.addAttribute(envelope.createName("type", "xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI), type);
                    paramElement.addTextNode(value);
                    return this;
                }
            }
            return addParameter(name, value, type);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest setParameter" + error);
        } catch (NullPointerException error) {
            throw new TSCellCommandRequestException("Received NullPointerException in TSCellCommandRequest setParameter" + error);
        }
    }

    public TSCellCommandRequest addParameters(final ParameterSet<? extends Parameter> parameterSet)
        throws TSCellCommandRequestException
    {
        if (parameterSet == null)
            return this;

        for (Parameter parameter : parameterSet.getParameters())
            addParameter(parameter);
        return this;
    }

    public TSCellCommandRequest setParameters(final ParameterSet<? extends Parameter> parameterSet)
        throws TSCellCommandRequestException
    {
        if (parameterSet == null)
            return this;

        for (Parameter parameter : parameterSet.getParameters())
            setParameter(parameter);
        return this;
    }

    public TSCellCommandRequest setPayload(final String payload)
        throws TSCellCommandRequestException
    {
        return setPayload(payload, "xsd:string");
    }

    public TSCellCommandRequest setPayload(final Parameter parameter)
        throws TSCellCommandRequestException
    {
        return setPayload(parameter.getValue().toString(), parameter.getValue().getXSIType());
    }

    public TSCellCommandRequest setPayload(final String value, final String type)
        throws TSCellCommandRequestException
    {
        try {
            if (payload == null) {
                Iterator payloadIterator = command.getChildElements(envelope.createName("payload", "cell", TSCellCommand.TS_NS_URI));
                if (payloadIterator.hasNext())
                    payload = (SOAPElement)payloadIterator.next();
                else
                    payload = command.addChildElement("payload", "cell", TSCellCommand.TS_NS_URI);
            }
            payload.removeContents();
            payload.addAttribute(envelope.createName("type", "xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI), type);
            payload.addTextNode(value);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest setPayload" + error);
        } catch (NullPointerException error) {
            throw new TSCellCommandRequestException("Received NullPointerException in TSCellCommandRequest setPayload" + error);
        }
        return this;
    }

    protected void initialize(final String commandName, String sessionId)
        throws TSCellCommandRequestException
    {
        SOAPBody body = null;
        this.commandName = commandName;
        // Create a SOAP message
        if (message == null)
            try {
                MessageFactory messageFactory = MessageFactory.newInstance();
                message = messageFactory.createMessage();
                envelope = message.getSOAPPart().getEnvelope();
                body = envelope.getBody();
            } catch (SOAPException error) {
                throw new TSCellCommandRequestException("Failed to create a new SOAPMessage", error);
            } catch (Exception error) {
                throw new TSCellCommandRequestException("Failed to create a new SOAPMessage", error);
            }

        // Set SOAPAction header with a default target
        message.getMimeHeaders().addHeader("SOAPAction", "urn:xdaq-application:lid=0");

        // Add Namespaces
        try {
            envelope.addNamespaceDeclaration("xsd", javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
            envelope.addNamespaceDeclaration("xsi", javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
            envelope.addNamespaceDeclaration("cell", TSCellCommand.TS_NS_URI);
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Failed to create add namespaces to SOAP message", error);
        }

        // Add the command element to the SOAP body
        try {
            if (commandName == null)
                command = body.addBodyElement(envelope.createName("genericRequest", "cell", TSCellCommand.TS_NS_URI));
            else
                command = body.addBodyElement(envelope.createName(commandName, "cell", TSCellCommand.TS_NS_URI));
        } catch (SOAPException error) {
            throw new TSCellCommandRequestException("Received SOAPException in TSCellCommandRequest initialize" + error);
        }

        // necessary attributes and parameters
        addAttribute("sid", sessionId);
        addAttribute("cid", commandName + "-" + UUID.randomUUID().toString());
        addAttribute("async", "false");
        addElement("callbackFun", commandName + "Response");
        // only required if async
        // addElement("callbackUrl", "NULL");
        // addElement("callbackUrn", "NULL");
    }

    public TSCellCommandRequest setAsync(boolean async, String url, String urn)
        throws TSCellCommandRequestException
    {
        setAttribute("async", async ? "true" : "false");
        setElement("callbackFun", commandName + "Response");
        setElement("callbackUrl", url);
        setElement("callbackUrn", urn);
        return this;
    }
}

package rcms.fm.app.rpc.ts;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import net.hep.cms.xdaqctl.XDAQMessage;
import net.hep.cms.xdaqctl.XDAQMessageException;
import org.w3c.dom.Document;
import rcms.statemachine.definition.Input;
import rcms.util.logger.RCMSLogger;

public class TSCellCommand extends XDAQMessage
{
    static RCMSLogger logger = new RCMSLogger(TSCellCommand.class);

    static final String TS_NS_URI = "urn:ts-soap:3.0";

    protected TSCellCommandRequest tsCellCommandRequest = null;
    protected TSCellCommandResponse tsCellCommandResponse = null;

    public TSCellCommand(final String commandName, String sessionId)
        throws TSCellCommandException, XDAQMessageException
    {
        super(commandName);
        tsCellCommandRequest = new TSCellCommandRequest(commandName, sessionId);
        tsCellCommandResponse = new TSCellCommandResponse();
        message = tsCellCommandRequest.getSOAPMessage();
    }

    public TSCellCommand(final Input input, String operation, String sessionId)
        throws TSCellCommandException, XDAQMessageException
    {
        super(input.getInputString());
        tsCellCommandRequest = new TSCellCommandRequest(input, operation, sessionId);
        tsCellCommandResponse = new TSCellCommandResponse();
        message = tsCellCommandRequest.getSOAPMessage();
    }

    public TSCellCommandRequest getRequest()
    {
        return tsCellCommandRequest;
    }

    public TSCellCommandResponse getResponse()
    {
        return tsCellCommandResponse;
    }

    public TSCellCommandResponse setResponse(Document document)
    {
        if (document == null)
            return null;
        SOAPMessage message = null;
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            message = messageFactory.createMessage();
            message.getSOAPBody().addDocument(document);
            tsCellCommandResponse.parse(message, tsCellCommandRequest.getCommandName() + "Response");
        } catch (SOAPException error) {
            logger.error("SOAPException during setResponse", error);
        } catch (TSCellCommandResponseException error) {
            logger.error("TSCellCommandRequestException during setResponse", error);
        }
        return tsCellCommandResponse;
    }
}

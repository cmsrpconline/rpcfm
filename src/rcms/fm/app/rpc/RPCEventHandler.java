package rcms.fm.app.rpc;

import java.util.Date;

import rcms.errorFormat.CMS.CMSError;
import rcms.fm.app.rpc.action.RPCAction;
import rcms.fm.app.rpc.action.RPCColdResetAction;
import rcms.fm.app.rpc.action.RPCConfigureAction;
import rcms.fm.app.rpc.action.RPCHaltAction;
import rcms.fm.app.rpc.action.RPCInitAction;
import rcms.fm.app.rpc.action.RPCPauseAction;
import rcms.fm.app.rpc.action.RPCPrepareTTSTestModeAction;
import rcms.fm.app.rpc.action.RPCRecoverAction;
import rcms.fm.app.rpc.action.RPCResetAction;
import rcms.fm.app.rpc.action.RPCResumeAction;
import rcms.fm.app.rpc.action.RPCStartAction;
import rcms.fm.app.rpc.action.RPCStopAction;
import rcms.fm.app.rpc.action.RPCTestTTSAction;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStates;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.fw.user.UserStateNotificationHandler;
import rcms.fm.resource.QualifiedGroup;
import rcms.statemachine.definition.State;
import rcms.util.logger.RCMSLogger;

/**
 * Main Event Handler class for RPC Function Manager.
 */
public class RPCEventHandler extends UserStateNotificationHandler
{

    static RCMSLogger logger = new RCMSLogger(RPCEventHandler.class);

    RPCFunctionManager functionManager = null;

    private QualifiedGroup qualifiedGroup = null;

    public RPCEventHandler()
        throws rcms.fm.fw.EventHandlerException
    {
        // this handler inherits UserStateNotificationHandler
        // so it is already registered for StateNotification events

        // Let's register also the StateEnteredEvent triggered when the FSM enters in a new state.
        subscribeForEvents(StateEnteredEvent.class);

        addAction(RPCStates.INITIALIZING,             "initAction");
        addAction(RPCStates.CONFIGURING,              "configureAction");
        addAction(RPCStates.HALTING,                  "haltAction");
        addAction(RPCStates.PREPARING_TTSTEST_MODE,   "prepareTTSTestModeAction");
        addAction(RPCStates.TESTING_TTS,              "testTTSAction");
        addAction(RPCStates.COLDRESETTING,            "coldResetAction");
        addAction(RPCStates.PAUSING,                  "pauseAction");
        addAction(RPCStates.RECOVERING,               "recoverAction");
        addAction(RPCStates.RESETTING,                "resetAction");
        addAction(RPCStates.RESUMING,                 "resumeAction");
        addAction(RPCStates.STARTING,                 "startAction");
        addAction(RPCStates.STOPPING,                 "stopAction");

        // addAction(RPCStates.FIXINGSOFTERROR,          "fixSoftErrorAction");
        // addAction(RPCStates.RUNNINGDEGRADED,          "runningDegradedAction");
        // addAction(RPCStates.RUNNINGSOFTERRORDETECTED, "runningSoftErrorDetectedAction");
        // addAction(RPCStates.RUNNING,                  "runningAction");
    }

    public RPCFunctionManager getFunctionManager()
    {
        return functionManager;
    }

    public RCMSLogger getLogger()
    {
        return logger;
    }

    public void sendCMSError(Exception error)
    {
        sendCMSError(error.getMessage());
    }

    public void sendCMSError(String errMsg)
    {
        CMSError error = functionManager.getErrorFactory().getCMSError();
        error.setDateTime(new Date().toString());
        error.setMessage(errMsg);

        // also put it in parameter ERROR_MSG
        functionManager.setErrorMessage(errMsg);

        // and send it
        try {
            functionManager.getParentErrorNotifier().sendError(error);
        } catch (Exception e) {
            logger.warn(functionManager.getClass().toString() + ": Failed to send error message " + errMsg);
        }
    }

    public void init()
        throws rcms.fm.fw.EventHandlerException
    {
        functionManager = (RPCFunctionManager) getUserFunctionManager();
        qualifiedGroup  = functionManager.getQualifiedGroup();
    }

    public void initAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCInitAction(this);
        action.handle(obj);
    }


    public void configureAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCConfigureAction(this);
        action.handle(obj);
    }


    public void haltAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCHaltAction(this);
        action.handle(obj);
    }


    public void prepareTTSTestModeAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCPrepareTTSTestModeAction(this);
        action.handle(obj);
    }


    public void testTTSAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCTestTTSAction(this);
        action.handle(obj);
    }


    public void coldResetAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCColdResetAction(this);
        action.handle(obj);
    }


    public void pauseAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCPauseAction(this);
        action.handle(obj);
    }


    public void recoverAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCRecoverAction(this);
        action.handle(obj);
    }


    public void resetAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCResetAction(this);
        action.handle(obj);
    }


    public void resumeAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCResumeAction(this);
        action.handle(obj);
    }


    public void startAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCStartAction(this);
        action.handle(obj);
    }


    public void stopAction(final Object obj)
        throws UserActionException
    {
        RPCAction action = new RPCStopAction(this);
        action.handle(obj);
    }

}

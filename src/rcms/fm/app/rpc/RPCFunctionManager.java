package rcms.fm.app.rpc;

import java.util.List;
import java.util.Map;

import rcms.fm.app.rpc.RPCErrorHandler;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.app.rpc.definition.RPCStateMachineDefinition;
import rcms.fm.app.rpc.definition.RPCTSStates;
import rcms.fm.app.rpc.definition.RPCTSInputs;
import rcms.fm.app.rpc.ts.TSCell;
import rcms.fm.app.rpc.ts.TSCellContainer;
import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.FunctionManagerParameter.Exported;
import rcms.fm.fw.parameter.type.ParameterType;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.fw.user.UserFunctionManager;
import rcms.fm.resource.QualifiedGroup;
import rcms.fm.resource.QualifiedResource;
import rcms.fm.resource.qualifiedresource.XdaqApplication;
import rcms.fm.resource.qualifiedresource.XdaqApplicationContainer;
import rcms.statemachine.definition.StateMachineDefinitionException;
import rcms.statemachine.definition.Transition;
import rcms.util.logger.RCMSLogger;

// temporary for checkIgnoreTS
import java.util.ArrayList;
import java.io.*;

/**
 * RPC Level 1 Function Manager
 */
public class RPCFunctionManager extends UserFunctionManager {

    static RCMSLogger logger = new RCMSLogger(RPCFunctionManager.class);

    public boolean ignoreTS = false; // temporary

    public void checkIgnoreTS()
    {
        try { // check ignore TS - temporary
            FileReader fileReader = new FileReader("/nfshome0/rpcpro/.functionmanagers/rpcFMIgnoreTS.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            if (line != null)
                ignoreTS = Boolean.parseBoolean(line);
            logger.info(ignoreTS ? "Ignoring RPC TS." : "Including RPC TS.");
        } catch (FileNotFoundException error) {
            logger.warn("Could not find /nfshome0/rpcpro/.functionmanagers/rpcFMIgnoreTS.txt to check if the RPC FM should ignore the RPC TS.  Including it." );
            ignoreTS = false;
        } catch (IOException error) {
            logger.warn("Could not read /nfshome0/rpcpro/.functionmanagers/rpcFMIgnoreTS.txt to check if the RPC FM should ignore the RPC TS.  Including it." );
            ignoreTS = false;
        }
    }

    public XdaqApplicationContainer xdaqApplicationContainer = null;
    public TSCellContainer tsCellContainer = null;

    public RPCFunctionManager()
    {
        addParameters();
    }

    public void createAction(ParameterSet<CommandParameter> parameters)
        throws UserActionException
    {
        logger.debug("createAction called.");
        for (Map.Entry<String, CommandParameter> parameter : parameters.getMap().entrySet()) {
            getParameterSet().put(new FunctionManagerParameter<ParameterType>("CREATED_WITH_" + parameter.getKey()
                                                                              , parameter.getValue().getValue()
                                                                              , Exported.READONLY));
        }
        logger.debug("createAction executed.");
    }

    public void destroyAction()
        throws UserActionException
    {
        logger.debug("destroyAction called.");
        qualifiedGroup.destroy();
        // previously, XdaqExecs were explicitly killed, but JobControl seems to do that automatically - verify
        logger.debug("destroyAction executed.");
    }

    private void addParameters()
    {
        parameterSet = RPCParameters.LVL_ONE_PARAMETER_SET;
    }

    public void init()
        throws StateMachineDefinitionException,
               rcms.fm.fw.EventHandlerException
    {
        logger.debug("init called.");
        setStateMachineDefinition(new RPCStateMachineDefinition());
        addEventHandler(new RPCEventHandler());
        addEventHandler(new RPCErrorHandler());
        logger.debug("init executed.");
    }

    public XdaqApplicationContainer getXdaqApplicationContainer()
    {
        return this.xdaqApplicationContainer;
    }

    public TSCellContainer getTSCellContainer()
    {
        if (ignoreTS)
            return new TSCellContainer(new ArrayList<TSCell>());
        return this.tsCellContainer;
    }

    public void fillResourceContainers()
    {
        List<QualifiedResource> xdaqApplicationList = getQualifiedGroup().seekQualifiedResourcesOfType(new XdaqApplication());
        setXdaqApplicationContainer(new XdaqApplicationContainer(xdaqApplicationList));

        List<QualifiedResource> tsCellList = getQualifiedGroup().seekQualifiedResourcesOfType(new TSCell());
        setTSCellContainer(new TSCellContainer(tsCellList));
    }

    public void setXdaqApplicationContainer(XdaqApplicationContainer xdaqApplications)
    {
        xdaqApplicationContainer = new XdaqApplicationContainer(xdaqApplications.getApplicationsOfClass("XdaqDccAccess"));
        if (xdaqApplicationContainer.getApplications().isEmpty())
            logger.warn("No valid Xdaq Applications found");
    }

    public void setTSCellContainer(TSCellContainer tsCells)
    {
        tsCellContainer = new TSCellContainer(tsCells.getCellsOfClass("rpcttscell::Cell"));
        if (tsCellContainer.getCells().isEmpty())
            logger.warn("No valid TS Cells found");
        for (TSCell cell : tsCellContainer.getCells()) {
            cell.addTransition(new Transition(RPCTSInputs.INITIALIZE, RPCTSStates.INITIAL   , RPCTSStates.HALTED)    , 10);
            cell.addTransition(new Transition(RPCTSInputs.COLDRESET , RPCTSStates.HALTED    , RPCTSStates.HALTED)    , 10);
            cell.addTransition(new Transition(RPCTSInputs.CONFIGURE , RPCTSStates.HALTED    , RPCTSStates.CONFIGURED), 120);
            cell.addTransition(new Transition(RPCTSInputs.START     , RPCTSStates.CONFIGURED, RPCTSStates.RUNNING)   , 10);
            cell.addTransition(new Transition(RPCTSInputs.PAUSE     , RPCTSStates.RUNNING   , RPCTSStates.PAUSED)    , 10);
            cell.addTransition(new Transition(RPCTSInputs.RESUME    , RPCTSStates.PAUSED    , RPCTSStates.RUNNING)   , 10);
            cell.addTransition(new Transition(RPCTSInputs.STOP      , null                  , RPCTSStates.CONFIGURED), 20);
            cell.addTransition(new Transition(RPCTSInputs.HALT      , null                  , RPCTSStates.HALTED)    , 10);
            cell.addTransition(new Transition(RPCTSInputs.RESET     , null                  , RPCTSStates.HALTED)    , 10);
            cell.addTransition(new Transition(RPCTSInputs.RECOVER   , null                  , RPCTSStates.HALTED)    , 10);
        }
    }

    public void setActionMessage(final String msg)
    {
        getParameterSet().put(new FunctionManagerParameter<StringT>(RPCParameters.ACTION_MSG
                                                                    , new StringT(msg)));
    }

    public void setErrorMessage(final String msg)
    {
        getParameterSet().put(new FunctionManagerParameter<StringT>(RPCParameters.ERROR_MSG
                                                                    , new StringT(msg)));
    }

    public void setStateParameter(final String msg)
    {
        getParameterSet().put(new FunctionManagerParameter<StringT>(RPCParameters.STATE
                                                                    , new StringT(msg)));
    }
}

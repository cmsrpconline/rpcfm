package rcms.fm.app.rpc;

import rcms.errorFormat.CMS.CMSError;
import rcms.fm.app.rpc.RPCEventHandler;
import rcms.fm.app.rpc.RPCFunctionManager;
import rcms.fm.app.rpc.definition.RPCParameters;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.fw.user.UserErrorHandler;
import rcms.fm.resource.QualifiedGroup;
import rcms.statemachine.definition.State;
import rcms.util.logger.RCMSLogger;

/**
 * Error Event Handler class for RPC Function Manager.
 */
public class RPCErrorHandler extends UserErrorHandler
{

    static RCMSLogger logger = new RCMSLogger(RPCErrorHandler.class);

    RPCFunctionManager functionManager = null;

    private QualifiedGroup qualifiedGroup = null;

    public RPCErrorHandler()
        throws rcms.fm.fw.EventHandlerException
    {
        // this handler inherits UserErrorHandler
        // so it is already registered for Error events

        // error handler
        addAction(State.ANYSTATE, "errorHandler");
    }

    public RPCFunctionManager getFunctionManager()
    {
        return functionManager;
    }

    public RCMSLogger getLogger()
    {
        return logger;
    }

    public void init()
        throws rcms.fm.fw.EventHandlerException
    {
        functionManager = (RPCFunctionManager) getUserFunctionManager();
        qualifiedGroup  = functionManager.getQualifiedGroup();

        // debug
        logger.debug("init() called: functionManager=" + functionManager);
    }

    public void errorHandler(Object obj)
        throws UserActionException
    {
        CMSError error = (CMSError)obj;

        if (error.getSeverity().equals(CMSError.SEVERITY_ERROR) ||
            error.getSeverity().equals(CMSError.SEVERITY_FATAL)) {

            String errMsg = "RPCErrorHandler received " + error.getIdentifier() + ":<br />" +
                (error.getMessage() == null ? null : error.getMessage().replaceAll("\n", "<br />")) +
                " <br/>For more information, check the log messages.";

            functionManager.setErrorMessage(errMsg);
        }
    }
}

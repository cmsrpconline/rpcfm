package rcms.fm.app.rpc.definition;

import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.ParameterException;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.*;
import rcms.statemachine.definition.Input;

/**
 * Definition of RPC Level 1 Function Manager Commands
 */
public class RPCTSInputs {
    public static final Input INITIALIZE = new Input("initialize");
    public static final Input COLDRESET  = new Input("coldReset");
    public static final Input CONFIGURE  = new Input("configure");
    public static final Input START      = new Input("start");
    // public static final Input FIXSOFTERROR = new Input("fixSoftError");
    public static final Input PAUSE      = new Input("pause");
    public static final Input RESUME     = new Input("resume");
    public static final Input STOP       = new Input("stop");
    public static final Input HALT       = new Input("halt");
    public static final Input RESET      = new Input("reset");
    public static final Input RECOVER    = new Input("recover");

    public static final Input SETERROR   = new Input("setError");

    public static final Input SETCONFIGURED = new Input("setConfigured");
    public static final Input SETRUNNING    = new Input("setRunning");
    // public static final Input SETRUNNINGDEGRADED = new Input("setRunningDegraded");
    // public static final Input SETRUNNINGSOFTERRORDETECTED = new Input("setRunningSoftErrorDetected");
    public static final Input SETPAUSED     = new Input("setPaused");
    public static final Input SETHALTED     = new Input("setHalted");

    static { // fill CONFIGURE ParameterSet
        CommandParameter<StringT> configureFedEnableMask
            = new CommandParameter<StringT>(RPCTSParameters.FED_ENABLE_MASK, new StringT(""));
        CommandParameter<IntegerT> configureRunNumber
            = new CommandParameter<IntegerT>(RPCTSParameters.RUN_NUMBER, new IntegerT(0));
        CommandParameter<BooleanT> configureUsePrimaryTCDS
            = new CommandParameter<BooleanT>(RPCTSParameters.USE_PRIMARY_TCDS, new BooleanT(true));
        CommandParameter<StringT> configureTPGKey
            = new CommandParameter<StringT>(RPCTSParameters.TPG_KEY, new StringT(""));

        CommandParameter<StringT> configureTTCMap // not received from L0, use dummy value
            = new CommandParameter<StringT>(RPCTSParameters.TTC_MAP, new StringT("{dummy=0}"));

        ParameterSet<CommandParameter> configureParameters = new ParameterSet<CommandParameter>();
        configureParameters.put(configureFedEnableMask);
        configureParameters.put(configureRunNumber);
        configureParameters.put(configureUsePrimaryTCDS);
        configureParameters.put(configureTPGKey);
        configureParameters.put(configureTTCMap);

        CONFIGURE.setParameters(configureParameters);
    }

    static { // fill START ParameterSet
        CommandParameter<IntegerT> startRunNumber
            = new CommandParameter<IntegerT>(RPCTSParameters.RUN_NUMBER, new IntegerT(0));

        ParameterSet<CommandParameter> startParameters = new ParameterSet<CommandParameter>();
        startParameters.put(startRunNumber);

        START.setParameters(startParameters);
    }

    public static Input translateConfigure(Input configure)
    {
        Input tsConfigure = new Input(CONFIGURE.getInputString());
        ParameterSet<CommandParameter> parameters = new ParameterSet<CommandParameter>();
        if (configure.getParameterSet() != null)
            {
                if (configure.getParameterSet().get(RPCParameters.FED_ENABLE_MASK) != null)
                    parameters.put(new CommandParameter<ParameterType>(RPCTSParameters.FED_ENABLE_MASK
                                                                       , configure.getParameterSet().get(RPCParameters.FED_ENABLE_MASK).getValue()));
                if (configure.getParameterSet().get(RPCParameters.RUN_NUMBER) != null)
                    parameters.put(new CommandParameter<ParameterType>(RPCTSParameters.RUN_NUMBER
                                                                       , configure.getParameterSet().get(RPCParameters.RUN_NUMBER).getValue()));
                if (configure.getParameterSet().get(RPCParameters.USE_PRIMARY_TCDS) != null)
                    parameters.put(new CommandParameter<ParameterType>(RPCTSParameters.USE_PRIMARY_TCDS
                                                                       , configure.getParameterSet().get(RPCParameters.USE_PRIMARY_TCDS).getValue()));
                if (configure.getParameterSet().get(RPCParameters.TPG_KEY) != null)
                    parameters.put(new CommandParameter<ParameterType>(RPCTSParameters.TPG_KEY
                                                                       , configure.getParameterSet().get(RPCParameters.TPG_KEY).getValue()));
                parameters.put(new CommandParameter<StringT>(RPCTSParameters.TTC_MAP, new StringT("{dummy=0}")));
            }
        tsConfigure.setParameters(parameters);
        return tsConfigure;
    }

    public static Input translateStart(Input start)
    {
        Input tsStart = new Input(START.getInputString());
        ParameterSet<CommandParameter> parameters = new ParameterSet<CommandParameter>();
        if (start.getParameterSet().get(RPCParameters.RUN_NUMBER) != null)
            parameters.put(new CommandParameter<ParameterType>(RPCTSParameters.RUN_NUMBER
                                                               , start.getParameterSet().get(RPCParameters.RUN_NUMBER).getValue()));
        tsStart.setParameters(parameters);
        return tsStart;
    }
}

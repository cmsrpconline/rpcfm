package rcms.fm.app.rpc.definition;

import rcms.fm.fw.user.UserStateMachineDefinition;
import rcms.statemachine.definition.State;
import rcms.statemachine.definition.StateMachineDefinitionException;

/**
 * This class defines the Finite State Machine for the RPC level 1 Function Manager.
 */
public class RPCStateMachineDefinition extends UserStateMachineDefinition {
    public RPCStateMachineDefinition() throws StateMachineDefinitionException {
        // steady states
        addState(RPCStates.INITIAL);
        addState(RPCStates.HALTED);
        addState(RPCStates.CONFIGURED);
        addState(RPCStates.RUNNING);
        // addState(RPCStates.RUNNINGDEGRADED);
        // addState(RPCStates.RUNNINGSOFTERRORDETECTED);
        addState(RPCStates.PAUSED);

        addState(RPCStates.ERROR);

        addState(RPCStates.TTSTEST_MODE);

        // transitional states
        addState(RPCStates.INITIALIZING);
        addState(RPCStates.COLDRESETTING);
        addState(RPCStates.CONFIGURING);
        addState(RPCStates.STARTING);
        // addState(RPCStates.FIXINGSOFTERROR);
        addState(RPCStates.PAUSING);
        addState(RPCStates.RESUMING);
        addState(RPCStates.STOPPING);
        addState(RPCStates.HALTING);
        addState(RPCStates.RESETTING);

        addState(RPCStates.RECOVERING);

        addState(RPCStates.PREPARING_TTSTEST_MODE);
        addState(RPCStates.TESTING_TTS);

        // initial state
        setInitialState(RPCStates.INITIAL);

        // inputs (commands)
        addInput(RPCInputs.INITIALIZE);
        addInput(RPCInputs.COLDRESET);
        addInput(RPCInputs.CONFIGURE);
        addInput(RPCInputs.START);
        // addInput(RPCInputs.FIXSOFTERROR);
        addInput(RPCInputs.PAUSE);
        addInput(RPCInputs.RESUME);
        addInput(RPCInputs.STOP);
        addInput(RPCInputs.HALT);
        addInput(RPCInputs.RESET);
        addInput(RPCInputs.RECOVER);

        addInput(RPCInputs.SETERROR);

        addInput(RPCInputs.PREPARE_TTSTEST_MODE);
        addInput(RPCInputs.TEST_TTS);

        // invisible commands needed for fully asynchronous behaviour
        addInput(RPCInputs.SETCONFIGURED);
        addInput(RPCInputs.SETRUNNING);
        // addInput(RPCInputs.SETRUNNINGDEGRADED);
        // addInput(RPCInputs.SETRUNNINGSOFTERRORDETECTED);
        addInput(RPCInputs.SETPAUSED);
        addInput(RPCInputs.SETHALTED);
        addInput(RPCInputs.SETTTSTEST_MODE);

        // invisible commands
        RPCInputs.SETCONFIGURED.setVisualizable(false);
        RPCInputs.SETRUNNING.setVisualizable(false);
        // RPCInputs.SETRUNNINGDEGRADED.setVisualizable(false);
        // RPCInputs.SETRUNNINGSOFTERRORDETECTED.setVisualizable(false);
        RPCInputs.SETPAUSED.setVisualizable(false);
        RPCInputs.SETHALTED.setVisualizable(false);

        RPCInputs.SETERROR.setVisualizable(false);

        RPCInputs.SETTTSTEST_MODE.setVisualizable(false);

        // State Transitions
        addTransition(RPCInputs.INITIALIZE, RPCStates.INITIAL   , RPCStates.INITIALIZING);
        addTransition(RPCInputs.COLDRESET , RPCStates.HALTED    , RPCStates.COLDRESETTING);
        addTransition(RPCInputs.CONFIGURE , RPCStates.HALTED    , RPCStates.CONFIGURING);
        addTransition(RPCInputs.START     , RPCStates.CONFIGURED, RPCStates.STARTING);
        addTransition(RPCInputs.PAUSE     , RPCStates.RUNNING   , RPCStates.PAUSING);
        addTransition(RPCInputs.RESUME    , RPCStates.PAUSED    , RPCStates.RESUMING);
        addTransition(RPCInputs.STOP      , RPCStates.RUNNING   , RPCStates.STOPPING);
        addTransition(RPCInputs.STOP      , RPCStates.PAUSED    , RPCStates.STOPPING);

        addTransition(RPCInputs.SETHALTED    , RPCStates.INITIALIZING , RPCStates.HALTED);
        addTransition(RPCInputs.SETHALTED    , RPCStates.COLDRESETTING, RPCStates.HALTED);
        addTransition(RPCInputs.SETCONFIGURED, RPCStates.CONFIGURING  , RPCStates.CONFIGURED);
        addTransition(RPCInputs.SETRUNNING   , RPCStates.STARTING     , RPCStates.RUNNING);
        addTransition(RPCInputs.SETPAUSED    , RPCStates.PAUSING      , RPCStates.PAUSED);
        addTransition(RPCInputs.SETRUNNING   , RPCStates.RESUMING     , RPCStates.RUNNING);
        addTransition(RPCInputs.SETCONFIGURED, RPCStates.STOPPING     , RPCStates.CONFIGURED);

        // Halt Transitions
        addTransition(RPCInputs.HALT     , RPCStates.CONFIGURED, RPCStates.HALTING);
        addTransition(RPCInputs.HALT     , RPCStates.RUNNING   , RPCStates.HALTING);
        addTransition(RPCInputs.HALT     , RPCStates.PAUSED    , RPCStates.HALTING);

        addTransition(RPCInputs.HALT, RPCStates.TTSTEST_MODE, RPCStates.HALTING);

        addTransition(RPCInputs.SETHALTED, RPCStates.HALTING   , RPCStates.HALTED);


        // Reset Transitions
        addTransition(RPCInputs.RESET    , RPCStates.HALTED      , RPCStates.RESETTING);
        addTransition(RPCInputs.RESET    , RPCStates.CONFIGURED  , RPCStates.RESETTING);
        addTransition(RPCInputs.RESET    , RPCStates.RUNNING     , RPCStates.RESETTING);
        addTransition(RPCInputs.RESET    , RPCStates.PAUSED      , RPCStates.RESETTING);
        addTransition(RPCInputs.RESET    , RPCStates.ERROR       , RPCStates.RESETTING);
        addTransition(RPCInputs.RESET    , RPCStates.TTSTEST_MODE, RPCStates.RESETTING);
        addTransition(RPCInputs.SETHALTED, RPCStates.RESETTING   , RPCStates.HALTED);

        // addTransition(RPCInputs.HALT, RPCStates.RUNNINGDEGRADED, RPCStates.HALTING);
        // addTransition(RPCInputs.HALT, RPCStates.RUNNINGSOFTERRORDETECTED, RPCStates.HALTING);
        // addTransition(RPCInputs.RESET, RPCStates.RUNNINGDEGRADED , RPCStates.RESETTING);
        // addTransition(RPCInputs.RESET, RPCStates.RUNNINGSOFTERRORDETECTED , RPCStates.RESETTING);
        // addTransition(RPCInputs.PAUSE, RPCStates.RUNNINGDEGRADED, RPCStates.PAUSING);
        // addTransition(RPCInputs.PAUSE, RPCStates.RUNNINGSOFTERRORDETECTED, RPCStates.PAUSING);
        // addTransition(RPCInputs.STOP, RPCStates.RUNNINGDEGRADED, RPCStates.STOPPING);
        // addTransition(RPCInputs.STOP, RPCStates.RUNNINGSOFTERRORDETECTED, RPCStates.STOPPING);
        // addTransition(RPCInputs.SETRUNNINGDEGRADED, RPCStates.STARTING, RPCStates.RUNNINGDEGRADED);
        // addTransition(RPCInputs.SETRUNNINGSOFTERRORDETECTED, RPCStates.STARTING, RPCStates.RUNNINGSOFTERRORDETECTED);
        // addTransition(RPCInputs.SETRUNNING, RPCStates.RUNNINGDEGRADED, RPCStates.RUNNING);
        // addTransition(RPCInputs.SETRUNNINGDEGRADED, RPCStates.RUNNING, RPCStates.RUNNINGDEGRADED);
        // addTransition(RPCInputs.SETRUNNINGSOFTERRORDETECTED, RPCStates.RUNNINGDEGRADED, RPCStates.RUNNINGSOFTERRORDETECTED);
        // addTransition(RPCInputs.SETRUNNINGSOFTERRORDETECTED, RPCStates.RUNNING, RPCStates.RUNNINGSOFTERRORDETECTED);
        // addTransition(RPCInputs.FIXSOFTERROR, RPCStates.RUNNING, RPCStates.FIXINGSOFTERROR);
        // addTransition(RPCInputs.FIXSOFTERROR, RPCStates.RUNNINGDEGRADED, RPCStates.FIXINGSOFTERROR);
        // addTransition(RPCInputs.FIXSOFTERROR, RPCStates.RUNNINGSOFTERRORDETECTED, RPCStates.FIXINGSOFTERROR);
        // addTransition(RPCInputs.SETRUNNING, RPCStates.FIXINGSOFTERROR, RPCStates.RUNNING);
        // addTransition(RPCInputs.SETRUNNINGDEGRADED, RPCStates.FIXINGSOFTERROR, RPCStates.RUNNINGDEGRADED);

        // Error Transitions
        addTransition(RPCInputs.SETERROR , State.ANYSTATE      , RPCStates.ERROR);
        addTransition(RPCInputs.RECOVER  , RPCStates.ERROR     , RPCStates.RECOVERING);
        addTransition(RPCInputs.SETHALTED, RPCStates.RECOVERING, RPCStates.HALTED);
        // addTransition(RPCInputs.SETCONFIGURED, RPCStates.RECOVERING, RPCStates.CONFIGURED);
        // addTransition(RPCInputs.SETPAUSED, RPCStates.RECOVERING, RPCStates.PAUSED);

        // State Transitions: TTS testing
        addTransition(RPCInputs.PREPARE_TTSTEST_MODE, RPCStates.HALTED                , RPCStates.PREPARING_TTSTEST_MODE);
        addTransition(RPCInputs.SETTTSTEST_MODE     , RPCStates.PREPARING_TTSTEST_MODE, RPCStates.TTSTEST_MODE);
        addTransition(RPCInputs.TEST_TTS            , RPCStates.TTSTEST_MODE          , RPCStates.TESTING_TTS);
        addTransition(RPCInputs.SETTTSTEST_MODE     , RPCStates.TESTING_TTS           , RPCStates.TTSTEST_MODE);
    }
}

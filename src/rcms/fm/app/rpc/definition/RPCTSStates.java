package rcms.fm.app.rpc.definition;

import rcms.statemachine.definition.State;

/**
 * Defined RPC Function Manager states.
 */
public final class RPCTSStates {
    public static final State INITIALIZING = new State("initializing");
    public static final State INITIAL = new State("initial");
    public static final State HALTING = new State("halting");
    public static final State HALTED = new State("halted");
    public static final State CONFIGURING = new State("configuring");
    public static final State CONFIGURED = new State("configured");
    public static final State STARTING = new State("starting");
    public static final State RESUMING = new State("resuming");
    public static final State RUNNING = new State("running");
    // public static final State RUNNINGDEGRADED = new State("runningDegraded");
    public static final State STOPPING = new State("stopping");
    public static final State PAUSING = new State("pausing");
    public static final State PAUSED = new State("paused");
    public static final State RECOVERING = new State("recovering");
    public static final State RESETTING = new State("resetting");
    public static final State ERROR = new State("error");
    public static final State PREPARING_TTSTEST_MODE = new State("preparingTTSTestMode");
    public static final State TTSTEST_MODE = new State("tTSTestMode");
    public static final State TESTING_TTS = new State("testingTTS");
    public static final State COLDRESETTING = new State("coldResetting");
    // public static final State RUNNINGSOFTERRORDETECTED = new State("runningSoftErrorDetected");
    // public static final State FIXINGSOFTERROR = new State("fixingSoftError");

    // Additional States for TS
    // public static final State CONFIGURED_NOT_SYNCHRONIZED = new State("configuredNotSynchronized");
    // public static final State SYNCHRONIZING = new State("synchronizing");
}

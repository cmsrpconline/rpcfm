package rcms.fm.app.rpc.definition;

public class RPCTSParameters {
    // Configure
    public static final String RUN_NUMBER = "Run Number";
    public static final String FED_ENABLE_MASK = "FEDMap";
    public static final String USE_PRIMARY_TCDS = "UsePrimaryTCDS";
    public static final String TPG_KEY = "KEY";

    public static final String TTC_MAP = "TTCMap"; // not received from L0, use dummy value

    // Start
    // public static final String RUN_NUMBER = "Run Number";
}

package rcms.fm.app.rpc.definition;

import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.ParameterException;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.*;
import rcms.statemachine.definition.Input;

/**
 * Definition of RPC Level 1 Function Manager Commands for the TS
 */
public class RPCInputs {
    public static final Input INITIALIZE = new Input("Initialize");
    public static final Input COLDRESET  = new Input("ColdReset");
    public static final Input CONFIGURE  = new Input("Configure");
    public static final Input START      = new Input("Start");
    // public static final Input FIXSOFTERROR = new Input("FixSoftError");
    public static final Input PAUSE      = new Input("Pause");
    public static final Input RESUME     = new Input("Resume");
    public static final Input STOP       = new Input("Stop");
    public static final Input HALT       = new Input("Halt");
    public static final Input RESET      = new Input("Reset");
    public static final Input RECOVER    = new Input("Recover");

    public static final Input SETERROR   = new Input("SetError");

    public static final Input PREPARE_TTSTEST_MODE = new Input("PrepareTTSTestMode");
    public static final Input TTSTEST_MODE = new Input("TTSTestMode"); // translation
    public static final Input TEST_TTS    = new Input("TestTTS");

    public static final Input SETCONFIGURED = new Input("SetConfigured");
    public static final Input SETRUNNING    = new Input("SetRunning");
    // public static final Input SETRUNNINGDEGRADED = new Input("SetRunningDegraded");
    // public static final Input SETRUNNINGSOFTERRORDETECTED = new Input("SetRunningSoftErrorDetected");
    public static final Input SETPAUSED     = new Input("SetPaused");
    public static final Input SETHALTED     = new Input("SetHalted");

    public static final Input SETTTSTEST_MODE = new Input("SetTTSTestMode");

    static { // fill INITIALIZE ParameterSet
        CommandParameter<IntegerT> initializeSid
            = new CommandParameter<IntegerT>(RPCParameters.SID, new IntegerT(0));
        CommandParameter<StringT> initializeGlobalConfigurationKey
            = new CommandParameter<StringT>(RPCParameters.GLOBAL_CONF_KEY, new StringT(""));

        ParameterSet<CommandParameter> initializeParameters = new ParameterSet<CommandParameter>();
        initializeParameters.put(initializeSid);
        initializeParameters.put(initializeGlobalConfigurationKey);

        INITIALIZE.setParameters(initializeParameters);
    }

    static { // fill CONFIGURE ParameterSet
        CommandParameter<StringT> configureFedEnableMask
            = new CommandParameter<StringT>(RPCParameters.FED_ENABLE_MASK, new StringT(""));
        CommandParameter<IntegerT> configureRunNumber
            = new CommandParameter<IntegerT>(RPCParameters.RUN_NUMBER, new IntegerT(-1));
        CommandParameter<StringT> configureRunKey
            = new CommandParameter<StringT>(RPCParameters.RUN_KEY, new StringT(""));

        CommandParameter<StringT> configureRunType
            = new CommandParameter<StringT>(RPCParameters.RUN_TYPE, new StringT(""));
        CommandParameter<BooleanT> configureUsePrimaryTCDS
            = new CommandParameter<BooleanT>(RPCParameters.USE_PRIMARY_TCDS, new BooleanT(true));
        CommandParameter<StringT> configureTPGKey
            = new CommandParameter<StringT>(RPCParameters.TPG_KEY, new StringT(""));

        ParameterSet<CommandParameter> configureParameters = new ParameterSet<CommandParameter>();
        configureParameters.put(configureFedEnableMask);
        configureParameters.put(configureRunNumber);
        configureParameters.put(configureRunKey);
        configureParameters.put(configureRunType);
        configureParameters.put(configureUsePrimaryTCDS);
        configureParameters.put(configureTPGKey);

        CONFIGURE.setParameters(configureParameters);
    }

    static { // fill START ParameterSet
        CommandParameter<IntegerT> startRunNumber
            = new CommandParameter<IntegerT>(RPCParameters.RUN_NUMBER, new IntegerT(0));

        ParameterSet<CommandParameter> startParameters = new ParameterSet<CommandParameter>();
        startParameters.put(startRunNumber);

        START.setParameters(startParameters);
    }

    static { // fill TEST_TTS ParameterSet
        CommandParameter<IntegerT> ttsTestFedid
            = new CommandParameter<IntegerT>(RPCParameters.TTS_TEST_FED_ID, new IntegerT(-1));
        CommandParameter<StringT> ttsTestMode
            = new CommandParameter<StringT>(RPCParameters.TTS_TEST_MODE, new StringT(""));
        CommandParameter<StringT> ttsTestPattern
            = new CommandParameter<StringT>(RPCParameters.TTS_TEST_PATTERN, new StringT(""));
        CommandParameter<IntegerT> ttsTestSequenceRepeat
            = new CommandParameter<IntegerT>(RPCParameters.TTS_TEST_SEQUENCE_REPEAT, new IntegerT(-1));

        ParameterSet<CommandParameter> ttsTestParameters = new ParameterSet<CommandParameter>();
        ttsTestParameters.put(ttsTestFedid);
        ttsTestParameters.put(ttsTestMode);
        ttsTestParameters.put(ttsTestPattern);
        ttsTestParameters.put(ttsTestSequenceRepeat);

        TEST_TTS.setParameters(ttsTestParameters);
    }
}

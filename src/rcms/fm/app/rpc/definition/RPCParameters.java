package rcms.fm.app.rpc.definition;

import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter.Exported;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.*;

public class RPCParameters {
    public static final String STATE = "STATE";

    public static final String ACTION_MSG = "ACTION_MSG";
    public static final String ERROR_MSG = "ERROR_MSG";
    public static final String COMPLETION = "COMPLETION";

    // Command parameters
    // Initialize
    public static final String SID = "SID";
    public static final String GLOBAL_CONF_KEY = "GLOBAL_CONF_KEY";
    // To be exported after initialize
    public static final String INITIALIZED_WITH = "INITIALIZED_WITH_";
    public static final String INITIALIZED_WITH_SID = INITIALIZED_WITH + SID;
    public static final String INITIALIZED_WITH_GLOBAL_CONF_KEY = INITIALIZED_WITH + GLOBAL_CONF_KEY;

    // Configure
    public static final String RUN_NUMBER = "RUN_NUMBER";
    public static final String RUN_KEY = "RUN_KEY";
    public static final String FED_ENABLE_MASK = "FED_ENABLE_MASK";
    public static final String TRIGGER_NUMBER_AT_PAUSE = "TRIGGER_NUMBER_AT_PAUSE";
    public static final String RUN_TYPE = "RUN_TYPE";
    public static final String USE_PRIMARY_TCDS = "USE_PRIMARY_TCDS";
    public static final String TPG_KEY = "TPG_KEY";
    // To be exported after configure
    public static final String CONFIGURED_WITH = "CONFIGURED_WITH_";
    public static final String CONFIGURED_WITH_FED_ENABLE_MASK = CONFIGURED_WITH + FED_ENABLE_MASK;
    public static final String CONFIGURED_WITH_RUN_KEY = CONFIGURED_WITH + RUN_KEY;
    public static final String CONFIGURED_WITH_RUN_NUMBER = CONFIGURED_WITH + RUN_NUMBER;
    public static final String CONFIGURED_WITH_RUN_TYPE = CONFIGURED_WITH + RUN_TYPE;
    public static final String CONFIGURED_WITH_USE_PRIMARY_TCDS = CONFIGURED_WITH + USE_PRIMARY_TCDS;
    public static final String CONFIGURED_WITH_TPG_KEY = CONFIGURED_WITH + TPG_KEY;

    // Start
    // public static final String RUN_NUMBER = "RUN_NUMBER";
    // To be exported after start
    public static final String STARTED_WITH = "STARTED_WITH_";
    public static final String STARTED_WITH_RUN_NUMBER = STARTED_WITH + RUN_NUMBER;

    // TTS Test
    public static final String TTS_TEST_FED_ID = "TTS_TEST_FED_ID";
    public static final String TTS_TEST_MODE = "TTS_TEST_MODE";
    public static final String TTS_TEST_PATTERN = "TTS_TEST_PATTERN";
    public static final String TTS_TEST_SEQUENCE_REPEAT = "TTS_TEST_SEQUENCE_REPEAT";
    // To be exported after tts_test
    public static final String TTS_TESTED_WITH = "TTS_TESTED_WITH_";
    public static final String TTS_TESTED_WITH_FED_ID = "TTS_TESTED_WITH_FED_ID";
    public static final String TTS_TESTED_WITH_MODE = "TTS_TESTED_WITH_MODE";
    public static final String TTS_TESTED_WITH_PATTERN = "TTS_TESTED_WITH_PATTERN";
    public static final String TTS_TESTED_WITH_SEQUENCE_REPEAT = "TTS_TESTED_WITH_SEQUENCE_REPEAT";

    // standard level 1 parameter set
    public static final ParameterSet<FunctionManagerParameter> LVL_ONE_PARAMETER_SET = new ParameterSet<FunctionManagerParameter>();

    static {
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(STATE, new StringT(""), Exported.READONLY));

        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(ACTION_MSG, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(ERROR_MSG, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<DoubleT>(COMPLETION, new DoubleT(-1), Exported.READONLY));

        // Initialize
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(INITIALIZED_WITH_SID, new IntegerT(-1), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(INITIALIZED_WITH_GLOBAL_CONF_KEY, new StringT(""), Exported.READONLY));
        // Configure
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_FED_ENABLE_MASK, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_RUN_KEY, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(CONFIGURED_WITH_RUN_NUMBER, new IntegerT(-1), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_RUN_TYPE, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<BooleanT>(CONFIGURED_WITH_USE_PRIMARY_TCDS , new BooleanT(true), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_TPG_KEY, new StringT(""), Exported.READONLY));
        // Start
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(STARTED_WITH_RUN_NUMBER, new IntegerT(-1), Exported.READONLY));

        // TTS Test
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(TTS_TESTED_WITH_FED_ID, new IntegerT(-1), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(TTS_TESTED_WITH_MODE, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(TTS_TESTED_WITH_PATTERN, new StringT(""), Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(TTS_TESTED_WITH_SEQUENCE_REPEAT, new IntegerT(-1), Exported.READONLY));
    }
}

package rcms.fm.app.rpc.tools;

import java.util.Map;

import org.w3c.dom.Element;
import rcms.fm.fw.parameter.Parameter;
import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.*;
import rcms.fm.fw.parameter.util.ParameterFactory;
import rcms.fm.fw.parameter.util.ParameterUtil;

public class ParameterTools
{
    public static ParameterSet<CommandParameter> translateCommandParameterSet(ParameterSet<CommandParameter> definition
                                                                              , ParameterSet<CommandParameter> input)
    {
        ParameterSet<CommandParameter> output = new ParameterSet<CommandParameter>();
        for (Map.Entry<String, CommandParameter> parameter : definition.getMap().entrySet()) {
            try {
                output.put(new CommandParameter<ParameterType>(parameter.getKey()
                                                               , input.get(parameter.getKey()).getValue()));
            } catch (Exception error) {
                output.put(new CommandParameter<ParameterType>(parameter.getKey()
                                                               , parameter.getValue().getValue()));
            }
        }
        return output;
    }

    public static Parameter createParameter(Element element)
        throws ParameterToolsException
    {
        String xsiType = element.getAttributeNS(javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI, "type");
        return createParameter(element.getLocalName()
                               , element.getTextContent()
                               , xsiType.substring(xsiType.indexOf(':') + 1));
    }

    public static Parameter createParameter(String name, String value, String type)
        throws ParameterToolsException
    {
        if (name == null || value == null || type == null)
            throw new ParameterToolsException("createParameter received null name, value or type");
        String canonicalType = String.class.getCanonicalName();
        switch (type) {
        case "boolean":
            canonicalType = BooleanT.class.getCanonicalName();
            break;
        case "integer":
        case "unsignedShort":
            canonicalType = IntegerT.class.getCanonicalName();
        break;
        case "short":
            canonicalType = ShortT.class.getCanonicalName();
            break;
        case "long":
        case "unsignedInt":
            canonicalType = LongT.class.getCanonicalName();
        break;
        case "byte":
            canonicalType = ByteT.class.getCanonicalName();
            break;
        case "float":
            canonicalType = FloatT.class.getCanonicalName();
            break;
        case "double":
            canonicalType = DoubleT.class.getCanonicalName();
            break;
        case "string":
            canonicalType = StringT.class.getCanonicalName();
            break;
        case "datetime":
            canonicalType = DateT.class.getCanonicalName();
            break;
        }

        if (ParameterTypeFactory.create(canonicalType) == null)
            throw new ParameterToolsException("createParameter null parametertype for type " + canonicalType);
        Parameter parameter = ParameterFactory.parameterFactory(canonicalType, name);
        if (parameter == null)
            throw new ParameterToolsException("ParameterFactory returned null parameter for type " + canonicalType + " and value " + value);

        ParameterUtil.setStringValue(parameter, value);
        return parameter;
    }
}

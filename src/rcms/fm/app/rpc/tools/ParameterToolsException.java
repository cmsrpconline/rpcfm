package rcms.fm.app.rpc.tools;

public class ParameterToolsException extends Exception
{
    public ParameterToolsException() {}
    public ParameterToolsException(String message) { super(message); }
    public ParameterToolsException(Throwable cause) { super(cause); }
    public ParameterToolsException(String message, Throwable cause) {
        super(message, cause);
    }
}
